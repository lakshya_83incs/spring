package com.example.TestProj;

import com.example.TestProj.controllers.FirstController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestProjApplication.class, args);


	}

}
