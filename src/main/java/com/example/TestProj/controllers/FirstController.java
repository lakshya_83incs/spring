package com.example.TestProj.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@ResponseBody
public class FirstController {

    @RequestMapping(path = "")
    public  String home(){
        return "hey there";
    }

    @RequestMapping(value = "bye", method = RequestMethod.GET)
    @ResponseBody
    public String good(){
        String html=   " <form method='post'>"+
      "<input type='text' id='login' name='username'>"+
      "<input type='password' id='password' name='password' placeholder='password'>"+
      "<input type='submit' value='Log In'>"+  "</form>";
        return html;
    }
    @RequestMapping(value = "bye", method = RequestMethod.POST)

    public String goodPost(@RequestParam String username, @RequestParam String password ){

        return username+" hello sir ur password "+ password;
    }

}
